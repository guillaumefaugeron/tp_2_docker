FROM python:latest

WORKDIR /app

EXPOSE 8080

COPY main.py /app/web/main.py

VOLUME /app/web/


CMD [ "python", "/app/web/main.py" ]
